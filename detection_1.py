from numpy.random import seed

seed(42)

def create_model(input_shape, outputs):
    from numpy import mean, std, ndarray
    from keras.models import Sequential
    from keras.layers import Dense, Flatten, Activation, Lambda
    from keras.layers.convolutional import Convolution2D
    from keras.layers.convolutional import MaxPooling2D
    
    model = Sequential([
        Convolution2D(10, (5, 5), padding='same', data_format='channels_last', input_shape=input_shape),
        Activation('relu'),
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        
        Convolution2D(30, (5, 5), padding='same', data_format='channels_last', input_shape=input_shape),
        Activation('relu'),
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        
        Flatten(),
        Dense(units=500),
        Activation('relu'),
        
        Dense(units=500),
        Activation('relu'),
        
        Dense(units=500),
        Activation('relu'),
        
        Dense(units=outputs),
        Activation('relu')
    ])
    
    model.compile(optimizer='rmsprop', loss='mse')
    return model


def train_detector(train_gt, train_img_dir, fast_train=False, model_name='facepoints_model.hdf5'):
    import numpy as np
    from ntpath import basename
    from os import listdir
    from os.path import join
    from skimage.io import imread_collection
    from skimage.transform import resize
    from pickle import dump
    
    P_POINTS_COUNT = 14 # count of principal points to search
    P_POINTS_COORDS_COUNT = 2 * P_POINTS_COUNT
    IMAGE_SHAPE = (100, 100, 3)
    SAMPLES_COUNT = len(train_gt)
    
    x_train = np.ndarray((SAMPLES_COUNT,) + IMAGE_SHAPE, dtype=np.float32)
    y_train = np.ndarray((SAMPLES_COUNT, P_POINTS_COORDS_COUNT), dtype=np.float32)
    images = imread_collection(join(train_img_dir, '*.jpg'), conserve_memory=True)
    sample = 0
    for image, filename in zip(images, images.files):
        coeff_x, coeff_y = IMAGE_SHAPE[1] / image.shape[1], IMAGE_SHAPE[0] / image.shape[0]
        x_train[sample, :] = resize(image, IMAGE_SHAPE, mode='reflect').astype(np.float32)
        y_train[sample, :] = train_gt[basename(filename)].astype(np.float32) * \
                                 np.tile([coeff_x, coeff_y], P_POINTS_COUNT)
        sample += 1
    
    means = np.ndarray(IMAGE_SHAPE)
    stds = np.ndarray(IMAGE_SHAPE)
    for i in range(3):
        means[:, :, i] = np.mean(x_train[:, :, :, i], axis=0)
        stds[:, :, i] = np.std(x_train[:, :, :, i], axis=0)
        x_train[:, :, :, i] = (x_train[:, :, :, i] - means[:, :, i]) / stds[:, :, i]
    with open('norm_data.pickle', 'wb') as f:
        dump((means, stds), f)
    
    # DEBUG
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)
    #######
    
    if fast_train:
        model = create_model(IMAGE_SHAPE, P_POINTS_COORDS_COUNT)
        model.fit(x_train, y_train, batch_size=100, epochs=1)
        model.save(model_name)
    else:
        model = create_model(IMAGE_SHAPE, P_POINTS_COORDS_COUNT)
        model.fit(x_train, y_train, batch_size=64, epochs=100)
        model.save(model_name)
        

def detect(model, test_img_dir, image_shape=(100, 100, 3)):
    import numpy as np
    from ntpath import basename
    from os.path import join
    from skimage.io import imread_collection
    from skimage.transform import resize
    from pickle import load
            

    images = imread_collection(join(test_img_dir, '*.jpg'), conserve_memory=True)
    x_test = np.ndarray((len(images),) + image_shape, dtype=np.float32)
    scales = {}
    filenames = []
    test = 0
    for image, filename in zip(images, images.files):
        base_name = basename(filename)
        image_h, image_w = image.shape[0:2]
        new_h, new_w = image_shape[0:2]
        scales[base_name] = (image_w / new_w, image_h / new_h)
        x_test[test, :] = resize(image, image_shape, mode='reflect').astype(np.float32)
        filenames.append(base_name)
        test += 1
    
    with open('norm_data.pickle', 'rb') as f:
        means, stds = load(f)
    for i in range(3):
        x_test[:, :, :, i] = (x_test[:, :, :, i] - means[:, :, i]) / stds[:, :, i]
    
    result = {}
    for filename, vec in zip(filenames, model.predict(x_test)):
        result[filename] = vec * np.tile(scales[filename], 14)
    return result
