from numpy.random import seed

seed(42)

from keras import backend as K
from keras.engine.topology import Layer
import numpy as np

# DEBUG
from pickle import load
#######


class Normalizer(Layer):
    def __init__(self, x_train, **kwargs):
        input_shape = x_train.shape[1:]
        self.means = np.ndarray(input_shape)
        self.stds = np.ndarray(input_shape)
        for i in range(3):
            self.means[:, :, i] = np.mean(x_train[:, :, :, i], axis=0)
            self.stds[:, :, i] = np.std(x_train[:, :, :, i], axis=0)
        super(Normalizer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(Normalizer, self).build(input_shape)

    def call(self, x):
        y = (x - self.means) / self.stds
        return y

    def compute_output_shape(self, input_shape):
        return input_shape
    
    def get_config(self):
        config = super(Normalizer, self).get_config()
        config['trainable'] = False
        return config


def create_model(x_train, input_shape, outputs):
    from numpy import mean, std, ndarray
    from keras.models import Sequential
    from keras.layers import Dense, Flatten, Activation, Lambda
    from keras.layers.convolutional import Convolution2D
    from keras.layers.convolutional import MaxPooling2D
    
    model = Sequential([
        Normalizer(x_train, input_shape=input_shape),
        Convolution2D(10, (5, 5), padding='same', data_format='channels_last', input_shape=input_shape),
        Activation('relu'),
        
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
        
        Convolution2D(20, (5, 5), padding='same', data_format='channels_last', input_shape=input_shape),
        Activation('relu'),
        
        Flatten(),
        Dense(units=500),
        Activation('relu'),
        
        Dense(units=outputs),
        Activation('relu')
    ])
    
    model.compile(optimizer='rmsprop', loss='mse')
    return model


def train_detector(train_gt, train_img_dir, fast_train=False, model_name='facepoints_model.hdf5'):
    import numpy as np
    from ntpath import basename
    from os import listdir
    from os.path import join
    from skimage.io import imread_collection
    from skimage.transform import resize
    
    P_POINTS_COUNT = 14 # count of principal points to search
    P_POINTS_COORDS_COUNT = 2 * P_POINTS_COUNT
    IMAGE_SHAPE = (100, 100, 3)
    SAMPLES_COUNT = len(train_gt)
    
    # DEBUG
    from pickle import load
    with open('x_train.pickle', 'rb') as f:
        x_train = load(f)
    with open('y_train.pickle', 'rb') as f:
        y_train = load(f)
    ######
    
    #x_train = np.ndarray((SAMPLES_COUNT,) + IMAGE_SHAPE, dtype=np.float32)
    #y_train = np.ndarray((SAMPLES_COUNT, P_POINTS_COORDS_COUNT), dtype=np.float32)
    #images = imread_collection(join(train_img_dir, '*.jpg'), conserve_memory=False)
    #sample = 0
    #for image, filename in zip(images, images.files):
    #    coeff_x, coeff_y = IMAGE_SHAPE[1] / image.shape[1], IMAGE_SHAPE[0] / image.shape[0]
    #    x_train[sample, :] = resize(image, IMAGE_SHAPE, mode='reflect').astype(np.float32)
    #    y_train[sample, :] = train_gt[basename(filename)].astype(np.float32) * \
    #                             np.tile([coeff_x, coeff_y], P_POINTS_COUNT)
    #    sample += 1
    
    # DEBUG
    import tensorflow as tf
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    tf.Session(config=config)
    #######
    
    if fast_train:
        model = create_model(x_train, IMAGE_SHAPE, P_POINTS_COORDS_COUNT)
        model.fit(x_train, y_train, batch_size=10, epochs=1)
        model.save(model_name)
    else:
        model = create_model(x_train, IMAGE_SHAPE, P_POINTS_COORDS_COUNT)
        model.fit(x_train, y_train, batch_size=64, epochs=20)
        model.save(model_name)
        

def detect(model, test_img_dir, image_shape=(100, 100, 3)):
    import numpy as np
    from ntpath import basename
    from os.path import join
    from skimage.io import imread_collection
    from skimage.transform import resize
            
    # DEBUG
    from pickle import load
    with open('x_test.pickle', 'rb') as f:
        x_test = load(f)
    with open('filenames.pickle', 'rb') as f:
        filenames = load(f)
    #######            

    #images = imread_collection(join(test_img_dir, '*.jpg'), conserve_memory=False)
    #x_test = np.ndarray((len(images),) + image_shape, dtype=np.float32)
    #filenames = []
    #test = 0
    #for image, filename in zip(images, images.files):
    #    x_test[test, :] = resize(image, image_shape, mode='reflect').astype(np.float32)
    #    filenames.append(basename(filename))
    #    test += 1
    
    return {i: vec for i, vec in zip(filenames, model.predict(x_test))}
