def read_csv(filename):
    from numpy import array
    res = {}
    with open(filename) as fhandle:
        next(fhandle)
        for line in fhandle:
            parts = line.rstrip('\n').split(',')
            coords = array([float(x) for x in parts[1:]], dtype='float64')
            res[parts[0]] = coords
    return res


import numpy as np
from ntpath import basename
from os import listdir
from os.path import join
from skimage.io import imread_collection
from skimage.transform import resize

train_dir = './test/00_input/train/'
train_img_dir = './test/00_input/train/images/'
test_img_dir = './test/00_input/test/images/'
train_gt = read_csv(join(train_dir, 'gt.csv'))

P_POINTS_COUNT = 14 # count of principal points to search
P_POINTS_COORDS_COUNT = 2 * P_POINTS_COUNT
IMAGE_SHAPE = (100, 100, 3)
SAMPLES_COUNT = len(train_gt)

x_train = np.ndarray((SAMPLES_COUNT,) + IMAGE_SHAPE, dtype=np.float32)
y_train = np.ndarray((SAMPLES_COUNT, P_POINTS_COORDS_COUNT), dtype=np.float32)
images = imread_collection(join(train_img_dir, '*.jpg'), conserve_memory=False)
sample = 0
for image, filename in zip(images, images.files):
    coeff_x, coeff_y = IMAGE_SHAPE[1] / image.shape[1], IMAGE_SHAPE[0] / image.shape[0]
    x_train[sample, :] = resize(image, IMAGE_SHAPE, mode='reflect').astype(np.float32)
    y_train[sample, :] = train_gt[basename(filename)].astype(np.float32) * \
                             np.tile([coeff_x, coeff_y], P_POINTS_COUNT)
    sample += 1

from pickle import dump

with open('x_train.pickle', 'wb') as f:
    dump(x_train, f)

with open('y_train.pickle', 'wb') as f:
    dump(y_train, f)


import numpy as np
from ntpath import basename
from os.path import join
from skimage.io import imread_collection
from skimage.transform import resize
        
image_shape = (100, 100, 3)
images = imread_collection(join(test_img_dir, '*.jpg'), conserve_memory=False)
x_test = np.ndarray((len(images),) + image_shape, dtype=np.float32)
filenames = []
scales = {}
test = 0
for image, filename in zip(images, images.files):
    base_name = basename(filename)
    x_test[test, :] = resize(image, image_shape, mode='reflect').astype(np.float32)
    image_h, image_w = image.shape[0:2]
    new_h, new_w = image_shape[0:2]
    scales[base_name] = (image_w / new_w, image_h / new_h)
    filenames.append(base_name)
    test += 1

with open('x_test.pickle', 'wb') as f:
    dump(x_test, f)
    
with open('scales.pickle', 'wb') as f:
    dump(scales, f)

from os import listdir
filenames = [filename for filename in listdir('./test/00_input/test/images/')]
with open('filenames.pickle', 'wb') as f:
    dump(filenames, f)
