def read_csv(filename):
    from numpy import array
    res = {}
    with open(filename) as fhandle:
        next(fhandle)
        for line in fhandle:
            parts = line.rstrip('\n').split(',')
            coords = array([float(x) for x in parts[1:]], dtype='float64')
            res[parts[0]] = coords
    return res


def read_img_shapes(gt_dir):
    from os.path import join
    img_shapes = {}
    with open(join(gt_dir, 'img_shapes.csv')) as fhandle:
        next(fhandle)
        for line in fhandle:
            parts = line.rstrip('\n').split(',')
            filename = parts[0]
            n_rows, n_cols = map(int, parts[1:])
            img_shapes[filename] = (n_rows, n_cols)
    return img_shapes


def compute_metric(detected, gt, img_shapes):
    res = 0.0
    for filename, coords in detected.items():
        n_rows, n_cols = img_shapes[filename]
        diff = (coords - gt[filename])
        diff[::2] /= n_cols
        diff[1::2] /= n_rows
        diff *= 100
        res += (diff ** 2).mean()
    return res / len(detected.keys())


def run_single_test():
    from detection import train_detector, detect
    from keras import backend as K
    from keras.models import load_model
    from os import environ
    from os.path import abspath, dirname, join
    from pickle import dump
    
    model = load_model('./facepoints_model.hdf5')
    detected = detect(model, './images_mirrored/')
    gt = read_csv('gt_rev.csv')
    img_shapes = read_img_shapes('./')
    error = compute_metric(detected, gt, img_shapes)
    
    with open('detected.pickle', 'wb') as f:
        dump(detected, f)
    
    print('Error: {}'.format(error))
    
    if environ.get('KERAS_BACKEND') == 'tensorflow':
        K.clear_session()


run_single_test()
