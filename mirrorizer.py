import pandas as pd
import numpy as np

from skimage import io
from os import listdir

INPUT_IMAGES_DIR = './images/'
OUTPUT_IMAGES_DIR = './images_mirrored/'

gt = pd.read_csv('gt.csv').as_matrix()
gt_rev = gt.copy()

for filename in listdir(INPUT_IMAGES_DIR):
    image = io.imread(INPUT_IMAGES_DIR + filename)
    width = image.shape[1]
    id = int(filename.split('.')[0])
    gt_rev[id, 1::2] = width - gt_rev[id, 1::2]
    io.imsave(OUTPUT_IMAGES_DIR + filename, np.flip(image, axis=1))

pd.DataFrame(gt_rev).to_csv('gt_rev.csv')
